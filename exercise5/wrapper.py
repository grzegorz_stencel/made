#Using python python libraries
import types
   

#val = ['element1','element2']
def argumentType(val):
    if isinstance(val, list):
        print "list"
    elif isinstance(val, tuple):
        print "tuple"
    elif isinstance(val, dict):
        print "dict"
    else:
        return False


val ={'key':'value'}
argumentType(val)
val = ('element1','element2')
argumentType(val)
val = ['element1','element2']
argumentType(val)
 
