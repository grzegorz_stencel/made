#Dummy way without having python libraries
def argumentType(val):
    try:
        val.append('1')
        return 'list'
    except:
        n=1
    try:
        val+tuple((1,2))
        return 'tuple'
    except:
        n=2
    try:
        val['das']=1
        return 'dict'
    except:
        return False

print '---dict--------'
val ={'key':'value'}
print val
print argumentType(val)

print '---tuple--------'
val = ('element1','element2')
print val
print argumentType(val)

print '-list----------'
val = ['element1','element2']
print val
print argumentType(val)
 
