#Translate the following while loop into a for loop (which does the same thing as the while loop):
i = 20 
while (i > 0): 
    print "i = ", i
    i =- 1 # there is an error or catch cause loop won't work i will be -1 after first loop :) suppose it should be # or i-=1 or i=i-1

#solution interpreting exercise strictly
print "sol1"
i=20
for j in range(i,0,-i):
    print "i = ", j

#Solution corrected:
print "Sol corrected"
i=20
for j in range(i,0,-1): #or reversed(range(i)):
    print "i = ", j
