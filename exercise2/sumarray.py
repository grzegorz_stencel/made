#  A program which sums the contents of a integer list or array.
#  NAIVE WAY to show understanding of iteration (e.g. loops and generators):
var = raw_input("Please integers separated by commas : ")
try:
    intarray=var.split(",")
    print "The sum of"
    print intarray
    print "is:"
    sumtotal=0
    for i in intarray:
        sumtotal+=int(i)
    
    print sumtotal
except:
    print "please enter integers separated by commas"

