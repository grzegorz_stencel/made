#using python funcions
from sets import Set

def intersecting_elements(a,b):
    intersecting=set(a).intersection(set(b))
    return {'intersecting_elements':list(intersecting),'count':len(intersecting)}#len(intersecting)}

#Result format : {'intersecting_elements':[elements],count:count_intersect}

a=[1,2,3,1,1]
b=[3,9,6,6,1]
print intersecting_elements(a,b)
