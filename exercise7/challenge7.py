def True_or_False(n):
    tru=[]
    fal=[]
    for a in n:
        b=n[a]
        for c in b:
            if(b[c]):
                tru.append(c)
            else:
                fal.append(c)
    return {'True':tru,'False':fal}
   
n = {0: {}, 1: {}, 2: {}, 3: {'a': {}, 'c': {}, 'b': {}, 'e': {}, 'd': {}, 'g': True, 'f': {}}, 4: {}, 5: {}, 6: {}, 7: {'a': True, 'c': {}, 'b': {}, 'e': {}, 'd': {}, 'g': {}, 'f':{}}, 8: {}, 9: {}}
print True_or_False(n)
